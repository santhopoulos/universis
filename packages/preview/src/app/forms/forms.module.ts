import {NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {AdvancedSearchComponent} from './components/advanced-search/advanced-search.component';
import {DropdownFormComponent} from './components/dropdown-form/dropdown-form.component';
import {ExpandableFormComponent} from './components/expandable-form/expandable-form.component';
import {FormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule, HttpHandler} from '@angular/common/http';
import {FormsRoutingModule} from './forms.routing.module';
import {SimpleFormComponent} from './components/simple-form/simple-form.component';
import {HighlightIncludeModule} from '../highlight/highlight.module';
import {AngularDataContext, DATA_CONTEXT_CONFIG, MostModule} from '@themost/angular';
import {BsDatepickerModule} from 'ngx-bootstrap';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [
    FormsModule,
    FormsRoutingModule,
    HttpClientModule,
    HighlightIncludeModule,
    BsDatepickerModule,
    CommonModule,
    MostModule
  ],
  declarations: [
    AdvancedSearchComponent,
    DropdownFormComponent,
    ExpandableFormComponent,
    SimpleFormComponent
  ],
  providers: [
    {
      provide: DATA_CONTEXT_CONFIG, useValue: {
        base: '/',
        options: {
          useMediaTypeExtensions: false,
          useResponseConversion: true
        }
      }
    },
    AngularDataContext,
    HttpClient
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class FormsUnifiedModule {
  constructor() {}
}
