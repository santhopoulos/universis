import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThesesRootComponent } from './theses-root.component';

describe('ThesesRootComponent', () => {
  let component: ThesesRootComponent;
  let fixture: ComponentFixture<ThesesRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThesesRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThesesRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
