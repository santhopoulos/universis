import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsPreviewGradingComponent } from './exams-preview-grading.component';

describe('ExamsPreviewGradingComponent', () => {
  let component: ExamsPreviewGradingComponent;
  let fixture: ComponentFixture<ExamsPreviewGradingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsPreviewGradingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsPreviewGradingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
