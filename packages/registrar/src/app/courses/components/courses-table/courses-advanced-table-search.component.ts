import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';
import { AdvancedTableSearchBaseComponent } from '@universis/ngx-tables';

@Component({
    selector: 'app-courses-advanced-table-search',
    templateUrl: './courses-advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class CoursesAdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent {

    constructor(_context: AngularDataContext, _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
        super();
    }
}
