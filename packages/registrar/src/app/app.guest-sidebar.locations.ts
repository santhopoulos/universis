export const REGISTRAR_GUEST_SIDEBAR_LOCATIONS = [
  {
    name: 'Sidebar.Dashboard',
    key: 'Sidebar.Dashboard',
    url: '/guest',
    icon: 'fa fa-archive',
    index: 0
  }
];
