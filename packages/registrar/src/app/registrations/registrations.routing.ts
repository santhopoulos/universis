import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegistrationsHomeComponent} from './components/registrations-home/registrations-home.component';
import {RegistrationsTableComponent} from './components/registrations-table/registrations-table.component';
import {RegistrationsPreviewComponent} from './components/registrations-preview/registrations-preview.component';
import {RegistrationsRootComponent} from './components/registrations-root/registrations-root.component';
import {RegistrationsPreviewGeneralComponent} from './components/registrations-preview/registrations-preview-general/registrations-preview-general.component';
import { RegistrationsPreviewLatestHistoryComponent } from './components/registrations-preview/registrations-preview-latest-history/registrations-preview-latest-history.component';
import { RegistrationTableConfigurationResolver, RegistrationTableSearchResolver } from './components/registrations-table/registration-table-config.resolver';
import {ActiveDepartmentResolver, CurrentAcademicYearResolver} from '../registrar-shared/services/activeDepartmentService.service';
import {
  RegistrationsPreviewGeneralConfigurationResolver,
  RegistrationsPreviewGeneralSearchResolver
} from './components/registrations-preview/registrations-preview-general/registrations-preview-general-config.resolver';
import {AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModalData} from '@universis/forms';

const routes: Routes = [
    {
        path: '',
        component: RegistrationsHomeComponent,
        data: {
            title: 'Registrations'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list/currentYear'
            },
            {
                path: 'list',
                pathMatch: 'full',
                redirectTo: 'list/currentYear'
            },
            {
                path: 'list/:list',
                component: RegistrationsTableComponent,
                data: {
                    title: 'Registrations List'
                },
                resolve: {
                  currentYear: CurrentAcademicYearResolver,
                  tableConfiguration: RegistrationTableConfigurationResolver,
                  searchConfiguration: RegistrationTableSearchResolver
                }
            }
        ]
    },
    {
        path: ':id',
        component: RegistrationsRootComponent,
        data: {
            title: 'Registrations Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'preview'
            },
            {
                path: 'preview',
                component: RegistrationsPreviewComponent,
                data: {
                    title: 'Registrations Preview'
                },
                children: [
                    {
                        path: '',
                        redirectTo: 'general'
                    },
                    {
                        path: 'general',
                        component: RegistrationsPreviewGeneralComponent,
                        data: {
                            title: 'Registrations.TabsInfo.viewTitle'
                        },
                        resolve: {
                          tableConfiguration: RegistrationsPreviewGeneralConfigurationResolver,
                          searchConfiguration: RegistrationsPreviewGeneralSearchResolver,
                          department: ActiveDepartmentResolver
                        },
                        children: [
                          {
                            path: 'item/:id/edit',
                            pathMatch: 'full',
                            component: AdvancedFormModalComponent,
                            outlet: 'modal',
                            data: <AdvancedFormModalData> {
                              model: 'StudentCourseClasses',
                              action: 'edit',
                              closeOnSubmit: true,
                              serviceQueryParams: {

                              }
                            },
                            resolve: {
                              data: AdvancedFormItemResolver
                            }
                          }
                        ]
                    },
                    {
                        path: 'history',
                        component: RegistrationsPreviewLatestHistoryComponent,
                        data: {
                            title: 'Registrations.TabsInfo.historyTitle'
                        }
                    }
                ]

            }
        ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class RegistrationsRoutingModule {
}
