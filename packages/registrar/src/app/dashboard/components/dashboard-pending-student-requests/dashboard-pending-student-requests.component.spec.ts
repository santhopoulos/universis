import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardPendingStudentRequestsComponent } from './dashboard-pending-student-requests.component';

describe('DashboardPendingStudentRequestsComponent', () => {
  let component: DashboardPendingStudentRequestsComponent;
  let fixture: ComponentFixture<DashboardPendingStudentRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardPendingStudentRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPendingStudentRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
