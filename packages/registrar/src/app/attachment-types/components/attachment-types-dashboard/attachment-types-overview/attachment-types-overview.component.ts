import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-attachment-types-overview',
  templateUrl: './attachment-types-overview.component.html',
  styleUrls: ['./attachment-types-overview.component.scss']
})
export class AttachmentTypesOverviewComponent implements OnInit {
  public model: any;
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.model = await this._context.model('AttachmentTypes')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .getItem();
  }

}
