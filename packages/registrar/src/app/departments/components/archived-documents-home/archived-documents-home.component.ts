import { Component, OnInit, ViewChild, OnDestroy, Input, ElementRef, ViewEncapsulation } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import { Subscription } from 'rxjs';
import {ActivatedUser, AuthGuard, ErrorService} from '@universis/common';
import {AppEventService} from '@universis/common';
declare var $: any;
import {AdvancedFilterValueProvider} from '@universis/ngx-tables';

@Component({
  selector: 'app-archived-documents-home',
  templateUrl: './archived-documents-home.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ArchivedDocumentsHomeComponent implements OnInit, OnDestroy {

  private paramSubscription: Subscription;
  public departmentDocumentNumberSeries: any[];
  @Input() department: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _router: Router,
              private _appEvent: AppEventService,
              private _advancedFilterValueProvider: AdvancedFilterValueProvider,
              private _authGuard: AuthGuard,
             private _activatedUser: ActivatedUser ) {
  }
  private userSubscription: Subscription;
  ngOnInit() {
    this.department = this._activatedRoute.snapshot.data.department.id;
    this.userSubscription = this._activatedUser.user.subscribe((user: any) => {
      this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
        this._context.model('DepartmentDocumentNumberSeries')
          .where('department').equal(this.department)
          .expand('parent')
          .orderBy('active desc,id desc')
          .getItems().then((items) => {
          this.departmentDocumentNumberSeries = items;
          items.map(x => {
            if (x.useParentIndex && x.parent) {
              const findIndex = items.findIndex(y => {
                return y.id === x.parent.id;
              });
              if (findIndex < 0) {
                // check access
                const canActivate = this._authGuard.
                  canActivateLocation(`/departments/current/documents/series/${x.parent.id}/items/parent`, user);
                if (canActivate && ((canActivate.mask & 1) === 1)) {
                  this.departmentDocumentNumberSeries.push(x.parent);
                }
              }
            }
          });
          if (this.departmentDocumentNumberSeries.length > 0) {
            this._router.navigateByUrl(`departments/current/documents/series/${this.departmentDocumentNumberSeries[0].id}/items/list`);
            this._advancedFilterValueProvider.values = {
              ...this._advancedFilterValueProvider.values,
              'documentSeries': this.departmentDocumentNumberSeries[0].id
            };
          }
        }).catch((err) => {
          console.log(err);
          this._errorService.showError(err);
        });
      });
    });
  }


  ngOnDestroy() {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }
  }


