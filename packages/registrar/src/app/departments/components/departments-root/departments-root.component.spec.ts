import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentsRootComponent } from './departments-root.component';

describe('DepartmentsRootComponent', () => {
  let component: DepartmentsRootComponent;
  let fixture: ComponentFixture<DepartmentsRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentsRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentsRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
