import {Component, OnInit, Input, OnDestroy, SimpleChanges} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';
import {AppEventService, DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-students-overview-snapshot',
  templateUrl: './students-overview-snapshot.component.html'
})
export class StudentsOverviewSnapshotComponent implements OnInit, OnDestroy  {

  public student;
  @Input() studentId: number;
  private subscription: Subscription;
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _modalService: ModalService,
              private _translateService: TranslateService,
              private _toastService: ToastService,
              private _errorService: ErrorService,
              private _appEvent: AppEventService) {
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.studentId) {
      if (changes.studentId.currentValue == null) {
        this.student = null;
        return;
      }
      this._context.model('Students')
        .where('id').equal(changes.studentId.currentValue)
        .select('id,departmentSnapshot')
        .expand('departmentSnapshot')
        .getItem()
        .then((value) => {
          this.student = value;
        });
    }
  }


  async ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  remove() {
    if (this.student && this.student.departmentSnapshot) {
      return this._modalService.showWarningDialog(
        this._translateService.instant('Students.RemoveDepartmentSnapshotAction.Title'),
        this._translateService.instant('Students.RemoveDepartmentSnapshotAction.Message'),
        DIALOG_BUTTONS.OkCancel).then( result => {
        if (result === 'ok') {
          const data = {
            id: this.student.id,
            departmentSnapshot : null
          };
          this._context.model('Students').save(data).then( () => {
            this._toastService.show(
              this._translateService.instant('Students.RemoveDepartmentSnapshotAction.Title'),
            this._translateService.instant('Students.RemoveDepartmentSnapshotAction.Success')

          );
          this.student.departmentSnapshot = null;
          }).catch( err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });

    }
  }
  }
