import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsOverviewCurrentgradesComponent } from './students-overview-currentgrades.component';

describe('StudentsOverviewCurrentgradesComponent', () => {
  let component: StudentsOverviewCurrentgradesComponent;
  let fixture: ComponentFixture<StudentsOverviewCurrentgradesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsOverviewCurrentgradesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsOverviewCurrentgradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
