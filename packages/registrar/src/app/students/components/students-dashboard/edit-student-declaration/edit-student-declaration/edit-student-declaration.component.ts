import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ConfigurationService, GradeScaleService, LoadingService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { AdvancedFormComponent } from '@universis/forms';
import { Subscription } from 'rxjs';
import { NumberFormatter } from '@universis/number-format';

@Component({
  selector: 'app-edit-student-declaration',
  templateUrl: './edit-student-declaration.component.html',
  styleUrls: ['./edit-student-declaration.component.scss'],
})
export class EditStudentDeclarationComponent
  extends RouterModalOkCancel
  implements OnInit, OnDestroy
{
  public studentDeclaration: any;
  public formName: string;
  public model: string;
  public formData: any;
  public lastError: any;
  private dataSubscription: Subscription;
  private formChangeSubscription: Subscription;
  private graduationGradeWrittenInWords: string;
  private graduationGradeWrittenInWordsComponent: any;

  @ViewChild('formComponent') formComponent: AdvancedFormComponent;
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _context: AngularDataContext,
    private _configurationService: ConfigurationService,
    private _loadingService: LoadingService,
    private _gradeScaleService: GradeScaleService
  ) {
    // call super constructor
    super(_router, _activatedRoute);
    // override-set class
    this.modalClass = 'modal-xl';
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(
      async (resolvedData) => {
        // get resolved student declaration data
        this.studentDeclaration = resolvedData && resolvedData.data;
        // get resolved form name
        this.formName = resolvedData && resolvedData.action;
        // and model
        this.model = resolvedData && resolvedData.model;
        // calculate graduation grade written in words (do not set form value)
        this.graduationGradeWrittenInWords = await
          this.calculateGraduationGradeWrittenInWords(false);
        // and assign to form for validations
        Object.assign(this.studentDeclaration, {
          validateGradeWrittenInWords: this.graduationGradeWrittenInWords,
        });
        // and initate form
        this.formData = this.studentDeclaration;
      }
    );

    this.formChangeSubscription = this.formComponent.form.change.subscribe(
      (event) => {
        if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
          // enable or disable button based on form status
          this.okButtonDisabled = !event.isValid;
        }
      }
    );
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }

  async ok() {
    try {
      // clear error, if any
      this.lastError = null;
      // get form data
      const studentDeclarationFormData =
        this.formComponent &&
        this.formComponent.form &&
        this.formComponent.form.formio &&
        this.formComponent.form.formio.data;
      if (studentDeclarationFormData == null) {
        return this.close();
      }
      // save
      await this._context.model(this.model).save(studentDeclarationFormData);
      // and close with reload fragment
      return this.close({ fragment: 'reload', skipLocationChange: true });
    } catch (err) {
      // log and set lastError
      console.error(err);
      this.lastError = err;
    }
  }

  onCustomEvent(event: any): void {
    if (event && event.type === 'calculateGradeWrittenInWords') {
      // calculate or set graduation grade written in words (set value)
      this.calculateGraduationGradeWrittenInWords(true).then(() => {
        //
      }).catch(err => {
        console.error(err);
        this.lastError = err;
      });
    }
  }

  async calculateGraduationGradeWrittenInWords(setFormComponentValue: boolean): Promise<string> {
    try {
      // if grade is already calculated
      if (this.graduationGradeWrittenInWords) {
        if (setFormComponentValue) {
          if (this.graduationGradeWrittenInWordsComponent == null) {
            this.graduationGradeWrittenInWordsComponent =
              this.getGradeWrittenInWordsComponent();
          }
          // set form value
          this.graduationGradeWrittenInWordsComponent.setValue(
            this.graduationGradeWrittenInWords
          );
        }
        // and exit
        return;
      }
      this._loadingService.showLoading();
      // adapt mappings for graduation edit for this procedure
      if (this.model === 'Students') {
        this.studentDeclaration.student = this.studentDeclaration;
      }
      if (
        !(
          this.studentDeclaration &&
          this.studentDeclaration.student &&
          this.studentDeclaration.student.studyProgram &&
          this.studentDeclaration.student.studyProgram.decimalDigits != null &&
          this.studentDeclaration.student.studyProgram.gradeScale &&
          this.studentDeclaration.student.studyProgram.gradeScale.scaleType !=
            null
        )
      ) {
        return;
      }
      // get decimal digits from student's study program
      const decimalDigits =
        this.studentDeclaration.student.studyProgram.decimalDigits;
      // get program grade scale type
      const scaleType =
        this.studentDeclaration.student.studyProgram.gradeScale.scaleType;
      // get current locale
      const locale = this._configurationService.currentLocale;
      // create number formatter
      const formatter = new NumberFormatter();
      // prepare decimal seperator
      const decimalSeparator = new Intl.NumberFormat(locale)
        .format(1.1)
        .substring(1, 2);
      // get grade scale with service
      const gradeScale = await this._gradeScaleService.getGradeScale(this.studentDeclaration.student.studyProgram.gradeScale.id);
      // get formatted grade
      const formattedGrade = gradeScale.format(this.studentDeclaration.graduationGrade);
      // and calculate grade written in words
      const graduationGradeWrittenInWords =
        scaleType === 0
          ? formatter
              .format(
                parseFloat(
                  formattedGrade.replace(
                    decimalSeparator,
                    '.'
                  )
                ),
                locale,
                decimalDigits
              )
              // ΝΟΤΕ: toLocaleUpperCase(locale) could be used here, but the current version of ts does not accept it.
              .toLocaleUpperCase()
              .normalize('NFD')
              .replace(/[\u0300-\u036f]/g, '')
          : formattedGrade;
      // set grade written in words
      this.graduationGradeWrittenInWords = graduationGradeWrittenInWords;
      if (setFormComponentValue) {
        if (this.graduationGradeWrittenInWordsComponent == null) {
          this.graduationGradeWrittenInWordsComponent =
            this.getGradeWrittenInWordsComponent();
        }
        // set form value
        this.graduationGradeWrittenInWordsComponent.setValue(
          this.graduationGradeWrittenInWords
        );
      }
      // reset data structure
      if (this.model === 'Students') {
        delete this.studentDeclaration.student;
      }
      // and return
      return graduationGradeWrittenInWords;
    } catch (err) {
      // log and set lastError
      console.error(err);
      this.lastError = err;
    } finally {
      this._loadingService.hideLoading();
    }
  }

  getGradeWrittenInWordsComponent() {
    const utils =
      this.formComponent &&
      this.formComponent.form &&
      this.formComponent.form.formio;
    if (utils && typeof utils.getComponent === 'function') {
      return utils.getComponent('graduationGradeWrittenInWords');
    }
  }

  async cancel() {
    // close
    return this.close();
  }
}
