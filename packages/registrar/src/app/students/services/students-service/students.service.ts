import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(
    private readonly _context: AngularDataContext
  ) { }

  async getProgramGroups(studentId: number) {
    return this._context.model('StudentProgramGroups')
      .where('student')
      .equal(studentId)
      .expand('programGroup')
      .take(-1)
      .getItems();
  }

  /**
   * Subscribe a student to a ptogramGroup
   * 
   * @param studentId The target student id
   * @param groupId The target ProgramGroup id
   * 
   */
  async addProgramGroup(studentId: number, groupId: number): Promise<any> {
    const group = await this._context.model('ProgramGroups')
      .where('id')
      .equal(groupId)
      .getItem();

    const exists = await this._context.model('StudentProgramGroups')
      .where('student').equal(studentId)
      .and('programGroup').equal(groupId)
      .select('id')
      .getItem();

    if (exists) {
      throw new Error('AlreadySubscribed');
    } else {
      const model = {
        object: studentId,
        groups: [group]
      };

      return this._context.model('UpdateStudentGroupActions').save(model);
    }
  }
}
