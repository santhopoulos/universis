import { Component, OnInit, ViewChild } from '@angular/core';
import { AdvancedSearchFormComponent, AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult, AdvancedTableSearchComponent } from '@universis/ngx-tables';
import * as CANDIDATE_UPLOAD_ACTIONS_CONFIG from './candidate-upload-actions.config.list.json';
import * as CANDIDATE_UPLOAD_ACTIONS_SEARCH_CONFIG from './candidate-upload-actions.search.list.json';


@Component({
  selector: 'app-candidate-upload-actions',
  templateUrl: './candidate-upload-actions.component.html',
  styleUrls: ['./candidate-upload-actions.component.scss']
})
export class CandidateUploadActionsComponent implements OnInit {

  public recordsTotal: number | string;
  public tableConfiguration: any;
  public searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  constructor() { }

  ngOnInit() {
    // set search config
    this.searchConfiguration = CANDIDATE_UPLOAD_ACTIONS_SEARCH_CONFIG;
    this.search.form = this.searchConfiguration;
    // and init
    this.search.ngOnInit();
    // set table config
    this.tableConfiguration = CANDIDATE_UPLOAD_ACTIONS_CONFIG;
    this.table.config = AdvancedTableConfiguration.cast(this.tableConfiguration);
    // reset search text
    this.advancedSearch.text = null;
    // reset table
    this.table.reset(false);
  }


  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

}
