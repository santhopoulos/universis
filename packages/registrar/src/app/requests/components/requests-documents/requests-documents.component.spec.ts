import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestsDocumentsComponent } from './requests-documents.component';

describe('RequestsDocumentsComponent', () => {
  let component: RequestsDocumentsComponent;
  let fixture: ComponentFixture<RequestsDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestsDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
