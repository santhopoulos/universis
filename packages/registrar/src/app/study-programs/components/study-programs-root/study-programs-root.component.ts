import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {cloneDeep} from 'lodash';
import * as STUDY_PROGRAMS_LIST_CONFIG from '../study-programs-table/study-programs-table.config.list.json';
import {TemplatePipe} from '@universis/common';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-study-programs-root',
  templateUrl: './study-programs-root.component.html',
})
export class StudyProgramsRootComponent implements OnInit, OnDestroy {

  public model: any;
  public studyProgramID: any;
  public actions: any[];
  public config: any;
  public allowedActions: any[];
  public edit: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _template: TemplatePipe) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studyProgramID = params.id;
      this.model = await this._context.model('StudyPrograms')
        .where('id').equal(this._activatedRoute.snapshot.params.id)
        .expand('department,studyLevel')
        .getItem();

      // @ts-ignore
      this.config = cloneDeep(STUDY_PROGRAMS_LIST_CONFIG as TableConfiguration);

      if (this.config.columns && this.model) {
        // get actions from config file
        this.actions = this.config.columns.filter(x => {
          return x.actions;
        })
          // map actions
          .map(x => x.actions)
          // get list items
          .reduce((a, b) => b, 0);

        // filter actions with student permissions
        this.allowedActions = this.actions.filter(x => {
          if (x.role) {
            if (x.role === 'action') {
              return x;
            }
          }
        });

        this.edit = this.actions.find(x => {
          if (x.role === 'edit') {
            x.href = this._template.transform(x.href, this.model);
            return x;
          }
        });

        this.actions = this.allowedActions;
        this.actions.forEach(action => {
          action.href = this._template.transform(action.href, this.model);
        });

      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
