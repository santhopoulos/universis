import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsRootComponent } from './study-programs-root.component';

describe('StudyProgramsRootComponent', () => {
  let component: StudyProgramsRootComponent;
  let fixture: ComponentFixture<StudyProgramsRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
