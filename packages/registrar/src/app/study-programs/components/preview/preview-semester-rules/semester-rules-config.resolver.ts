import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class SemesterRulesConfigurationResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./preview-semester-rules.config.${route.params.list}.json`)
      .catch(err => {
        return import(`packages/registrar/src/app/study-programs/components/preview/preview-semester-rules/preview-semester-rules.config.list.json`);
      });
  }
}

export class SemesterRulesSearchResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./semester-rules.search.${route.params.list}.json`)
      .catch(err => {
        return import(`./semester-rules.search.list.json`);
      });
  }
}

