import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './components/list/list.component';
import { AdvancedFormsModule } from '@universis/forms';
import { ElementsModule } from '../elements/elements.module';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ErrorModule, SharedModule } from '@universis/common';
import { TablesModule } from '@universis/ngx-tables';
import { RouterModule, Routes } from '@angular/router';
import { MostModule } from '@themost/angular';
import { RouterModalModule } from '@universis/common/routing';
import { NgArrayPipesModule } from 'ngx-pipes';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ImportComponent } from './components/import/import.component';
import { DegreeTemplatesSharedModule } from './degree-templates-shared.module';
import { SpecializationListComponent } from './components/specializations/list.component';

const routes: Routes = [
  {
    path: 'list',
    component: ListComponent
  },
  {
    path: 'specializations',
    component: SpecializationListComponent
  }
];

@NgModule({
  imports: [
    ElementsModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    TranslateModule,
    TablesModule,
    SharedModule,
    ErrorModule,
    MostModule,
    AdvancedFormsModule,
    RouterModalModule,
    NgArrayPipesModule,
    NgxDropzoneModule,
    DegreeTemplatesSharedModule
  ],
  declarations: [
    ListComponent,
    ImportComponent,
    SpecializationListComponent
  ],
  schemas: [
     CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class DegreeTemplatesModule { }
