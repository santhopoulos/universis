import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ClientDataQueryable } from '@themost/client';
import { ErrorService, LoadingService, ModalService } from '@universis/common';
import {
  AdvancedRowActionComponent,
  AdvancedSearchFormComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult,
  AdvancedTableSearchComponent,
} from '@universis/ngx-tables';
import { Observable, Subscription } from 'rxjs';
import * as GRADUATIONS_DECLARED_STUDENTS_TABLE_CONFIG from './graduations-declared-students.config.json';
import * as GRADUATIONS_DECLARED_STUDENTS_SEARCH_CONFIG from './graduations-declared-students.search.config.json';

@Component({
  selector: 'app-graduations-declared-students',
  templateUrl: './graduations-declared-students.component.html',
  styleUrls: ['./graduations-declared-students.component.scss'],
})
export class GraduationsDeclaredStudentsComponent implements OnInit, OnDestroy {
  private selectedItems = [];
  private paramSubscription: Subscription;

  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  public recordsTotal: any;

  constructor(
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _context: AngularDataContext,
    private readonly _loadingService: LoadingService,
    private readonly _errorService: ErrorService,
    private readonly _modalService: ModalService,
    private readonly _translateService: TranslateService
  ) {}

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
      // set query
      this.table.query = this._context
        .model('GraduationRequestActions')
        .where('graduationEvent')
        .equal(params.id)
        .and('student/declaration/id')
        .notEqual(null)
        .prepare();
      // set search form configuration
      this.search.form = GRADUATIONS_DECLARED_STUDENTS_SEARCH_CONFIG;
      // and init search
      this.search.ngOnInit();
      // set table configuration
      this.table.config = AdvancedTableConfiguration.cast(
        GRADUATIONS_DECLARED_STUDENTS_TABLE_CONFIG
      );
      // and init table
      this.table.ngOnInit();
    });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  async changeDeclarationStatus(status: string) {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      const validationPromises = items.map(async (item) => {
        return {
          id: item.id,
          studentId: item.studentId,
          declareAction: await this.getActiveDeclareAction(item.id),
        };
      });
      // filter items with active declare actions
      this.selectedItems = (await Promise.all(validationPromises)).filter(
        (item) => !!item.declareAction
      );
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle:
            status === 'CompletedActionStatus'
              ? 'Graduations.CompleteDeclarationAction.Title'
              : 'Graduations.CancelDeclarationAction.Title',
          description:
            status === 'CompletedActionStatus'
              ? 'Graduations.CompleteDeclarationAction.Description'
              : 'Graduations.CancelDeclarationAction.Description',
          errorMessage:
            status === 'CompletedActionStatus'
              ? 'Graduations.CompleteDeclarationAction.CompletedWithErrors'
              : 'Graduations.CancelDeclarationAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeChangeDeclarationStatus(status),
        },
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.',
      });
    }
  }

  executeChangeDeclarationStatus(status: string) {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0,
      };
      this.refreshAction.emit({
        progress: 1,
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100),
            });
            // get student declare action
            const studentDeclareAction = item.declareAction;
            // change status
            studentDeclareAction.actionStatus = {
              alternateName: status,
            };
            // and save
            await this._context
              .model('StudentDeclareActions')
              .save(studentDeclareAction);
            result.success += 1;
            try {
              await this.table.fetchOne({
                id: item.id,
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.error(err);
            result.errors += 1;
          }
        }
      })()
        .then(() => {
          observer.next(result);
        })
        .catch((err) => {
          observer.error(err);
        });
    });
  }

  async setLastObligationDate() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // filter items with active declare actions
      this.selectedItems = items;
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Graduations.SetLastObligationDateAction.Title',
          description: 'Graduations.SetLastObligationDateAction.Description',
          errorMessage:
            'Graduations.SetLastObligationDateAction.CompletedWithErrors',
          formTemplate:
            this.selectedItems && this.selectedItems.length
              ? 'GraduationEvents/setLastObligationDate'
              : null,
          refresh: this.refreshAction,
          execute: this.executeSetLastObligationDate(),
        },
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.',
      });
    }
  }

  executeSetLastObligationDate() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0,
      };
      const component = this._modalService.modalRef
        .content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (
        !(
          data &&
          data.lastObligationDate != '' &&
          data.lastObligationDate != null
        )
      ) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1,
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100),
            });
            // validate lastObligationDate against inscription date and graduation date
            // TODO: Consider more specific validations with graduation year
            const lastObligation = new Date(data.lastObligationDate);
            const inscriptionDate = item.inscriptionDate
              ? new Date(item.inscriptionDate)
              : null;
            const graduationDate = item.eventStartDate
              ? new Date(item.eventStartDate)
              : null;
            if (
              (inscriptionDate &&
                lastObligation.getTime() <= inscriptionDate.getTime()) ||
              (graduationDate &&
                lastObligation.getTime() > graduationDate.getTime())
            ) {
              throw new Error(
                this._translateService.instant(
                  'Graduations.InvalidLastObligationDate'
                )
              );
            }
            // get student declare action
            const studentDeclareAction = await this._context
              .model('StudentDeclareActions')
              .where('initiator')
              .equal(item.id)
              .expand('actionStatus')
              .orderByDescending('id')
              .getItem();
            // if it exists
            if (studentDeclareAction) {
              // set last obligation date
              studentDeclareAction.lastObligationDate = data.lastObligationDate;
              // and update
              // note: If the action is active, student is also updated
              await this._context
                .model('StudentDeclareActions')
                .save(studentDeclareAction);
            }
            // try to find graduate action
            const graduateAction = await this._context
              .model('StudentGraduateActions')
              .where('initiator')
              .equal(item.id)
              .orderByDescending('id')
              .getItem();
            // if it exists
            if (graduateAction) {
              // apply last obligation date
              graduateAction.lastObligationDate = data.lastObligationDate;
              // and save
              await this._context
                .model('StudentGraduateActions')
                .save(graduateAction);
            }
            if (
              studentDeclareAction == null ||
              studentDeclareAction.actionStatus.alternateName !==
                'ActiveActionStatus'
            ) {
              // update student
              const updateStudent = {
                id: item.studentId,
                lastObligationDate: data.lastObligationDate,
              };
              await this._context.model('Students').save(updateStudent);
            }
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id,
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })()
        .then(() => {
          observer.next(result);
        })
        .catch((err) => {
          observer.error(err);
        });
    });
  }

  async changeGraduationStatus(status: string) {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      const validationPromises = items.map(async (item) => {
        return {
          id: item.id,
          studentId: item.studentId,
          graduateAction: await this.getActiveGraduateAction(item.id),
        };
      });
      // filter items with active graduate action
      this.selectedItems = (await Promise.all(validationPromises)).filter(
        (item) => !!item.graduateAction
      );
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle:
            status === 'CompletedActionStatus'
              ? 'Graduations.CompleteGraduationAction.Title'
              : 'Graduations.CancelGraduationAction.Title',
          description:
            status === 'CompletedActionStatus'
              ? 'Graduations.CompleteGraduationAction.Description'
              : 'Graduations.CancelGraduationAction.Description',
          errorMessage:
            status === 'CompletedActionStatus'
              ? 'Graduations.CompleteGraduationAction.CompletedWithErrors'
              : 'Graduations.CancelGraduationAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeChangeGraduationStatus(status),
        },
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.',
      });
    }
  }

  executeChangeGraduationStatus(status: string) {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0,
      };
      this.refreshAction.emit({
        progress: 1,
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100),
            });
            // find StudentGraduateAction for the student by initiator
            const studentGraduateAction = item.graduateAction;
            // try to change status
            studentGraduateAction.actionStatus = {
              alternateName: status,
            };
            await this._context
              .model('StudentGraduateActions')
              .save(studentGraduateAction);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })()
        .then(() => {
          this.table.fetch();
          observer.next(result);
        })
        .catch((err) => {
          observer.error(err);
        });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = [
            'id',
            'student/id as studentId',
            'student/inscriptionDate as inscriptionDate',
            'graduationEvent/startDate as eventStartDate',
          ];
          // query items
          const queryItems = await lastQuery.select
            .apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter((item) => {
              return (
                this.table.unselected.findIndex((x) => {
                  return x.id === item.id;
                }) < 0
              );
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              studentId: item.studentId,
              inscriptionDate: item.inscriptionDate,
              eventStartDate: item.eventStartDate,
            };
          });
        }
      }
    }
    return items;
  }

  getActiveDeclareAction(initiator: number | string): Promise<any> {
    return this.getDeclareAction(initiator, 'ActiveActionStatus');
  }

  getCompletedDeclareAction(initiator: number | string): Promise<any> {
    return this.getDeclareAction(initiator, 'CompletedActionStatus');
  }

  getDeclareAction(initiator: number | string, status: string): Promise<any> {
    if (initiator == null || status == null) {
      return Promise.resolve();
    }
    return this._context
      .model('StudentDeclareActions')
      .where('initiator')
      .equal(initiator)
      .and('actionStatus/alternateName')
      .equal(status)
      .orderByDescending('id')
      .getItem();
  }

  getActiveGraduateAction(initiator: number | string): Promise<any> {
    if (initiator == null) {
      return Promise.resolve();
    }
    return this._context
      .model('StudentGraduateActions')
      .where('initiator')
      .equal(initiator)
      .and('actionStatus/alternateName')
      .equal('ActiveActionStatus')
      .getItem();
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }
}
